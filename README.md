# Reinforcement Learning for Bipedal walking robot.
This repository contains the simulation architecture based in Gazebo environment for implementing reinforcement learning algorithm, DDPG for generating bipedal walking patterns for the robot.

## Planar Bipedal walking robot in Gazebo environment using Deep Deterministic Policy Gradient(DDPG).
The autonomous walking of the bipedal walking robot is achieved using reinforcement learning algorithm called Deep Deterministic Policy Gradient(DDPG). DDPG utilises the actor-critic learning framework for learning controls in continuous action spaces.

## Dependencies & Packages:
- Ubuntu 16.04
- ROS Kinetic
- Gazebo 7
- TensorFlow: 1.1.0 [with GPU support]
- gym: 0.9.3
- Python 2.7

## File setup:
- walker_gazebo contains the robot model(both **.stl** files & **.urdf** file) and also the gazebo **launch** file.

- walker_controller contains the reinforcement learning implementation of **DDPG algorithm** for control of the bipedal walking robot.

## Sources:
- Lillicrap, Timothy P., et al. Continuous control with deep reinforcement learning. arXiv preprint arXiv:1509.02971 (2015).
- Silver, David, et al. Deterministic Policy Gradient Algorithms. ICML (2014).

## Project Collaborator(s):
Arun Kumar (arunkumar12@iisc.ac.in) & Dr. S N Omkar (omkar@iisc.ac.in)

